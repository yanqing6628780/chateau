<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('wine', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('barcode', 50);
			$table->string('productName', 50);
			$table->string('productName_en', 100);
			$table->string('productImage', 100)->nullable();
			$table->string('chateau', 100)->nullable();
			$table->string('country', 100)->nullable();
			$table->string('produceArea', 100)->nullable();
			$table->string('level', 100)->nullable();
			$table->string('a_img_one', 100)->nullable();
			$table->string('a_img_two', 100)->nullable();
			$table->string('a_img_three', 100)->nullable();
			$table->string('a_img_four', 100)->nullable();
			$table->string('a_img_five', 100)->nullable();
			$table->string('a_img_six', 100)->nullable();
			$table->string('a_img_seven', 100)->nullable();
			$table->string('a_img_eight', 100)->nullable();
			$table->string('grape_variety', 100)->nullable();
			$table->string('grape_year', 100)->nullable();
			$table->string('alcohol_concentration', 100)->nullable();
			$table->string('brew_time', 100)->nullable();
			$table->string('ripening_time', 100)->nullable();
			$table->string('ripening_color', 100)->nullable();
			$table->string('glory_img_one', 100)->nullable();
			$table->string('glory_tittle_one', 100)->nullable();
			$table->string('glory_content_one', 100)->nullable();
			$table->string('glory_img_two', 100)->nullable();
			$table->string('glory_tittle_two', 100)->nullable();
			$table->string('glory_content_two', 100)->nullable();
			$table->string('glory_img_three', 100)->nullable();
			$table->string('glory_title_three', 100)->nullable();
			$table->string('glory_content_three', 100)->nullable();
			$table->string('catering_img_one', 100)->nullable();
			$table->string('catering_title_one', 100)->nullable();
			$table->string('catering_img_two', 100)->nullable();
			$table->string('catering_title_two', 100)->nullable();
			$table->string('catering_img_three', 100)->nullable();
			$table->string('catering_title_three', 100)->nullable();
			$table->string('catering_img_four', 100)->nullable();
			$table->string('catering_title_four', 100)->nullable();
			$table->string('taste_temperature', 100)->nullable();
			$table->string('sober_time', 100)->nullable();
			$table->string('save_way', 100)->nullable();
			$table->string('drink_date', 100)->nullable();
			$table->text('introduce', 100)->nullable();
			$table->string('qc_image', 100)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('wine');
	}

}
