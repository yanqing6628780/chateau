<!DOCTYPE html>
<html lang="zh-CN">
<head>
<title>{{$product->productName}}</title>
<meta charset="utf-8">
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="{{$product->chateau}}" />
<meta name="description" content="{{$product->chateau}}" />
<link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/assets/bootstrap/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/assets/fullpage/jquery.fullpage.min.css">
<link rel="stylesheet" href="/assets/style.css">

<script src="/assets/jquery.min.js"></script>
<script src="/assets/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<div id="fullpage">
    <section id="page1" class="page section">
        <h2 class="fcn text-center title-h2">{{$product->productName}}</h2>
        <h1 class="fcn text-center title-h1">{{$product->chateau}}</h1>
        <h3 class="fcn text-center title-h3" style="margin-bottom: 10px">{{$product->productName_en}}</h3>
        <div class="container content">
            <div class="row">
                <div class="col-md-3 wine">
                    <img class="img-responsive img-size" src="/{{$product->productImage}}">
                </div>
                <div class="col-md-9 wine-content">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="row">
                                <div class="col-md-12">
                                    <img class="img-responsive img-size" src="/{{$product->a_img_one}}">
                                </div>
                                <div class="col-md-12 page01-fonttop">
                                    <p class="txt fcn">国家：{{$product->country}}  </p>
                                    <p class="txt fcn">产区：{{$product->produceArea}}</p>
                                    <p class="txt fcn">级别：{{$product->level}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="row gallery">
                                <div class="col-md-12">
                                    <img class="img-responsive" src="/{{$product->a_img_two}}">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 "><img class="img-responsive" src="/{{$product->a_img_three}}"></div>
                                <div class="col-md-6 col-sm-6 col-xs-6 con-left-top"><img class="img-responsive" src="/{{$product->a_img_four}}"></div>
                                <div class="col-md-6 col-sm-6 col-xs-6 "><img class="img-responsive" src="/{{$product->a_img_five}}"></div>
                                <div class="col-md-6 col-sm-6 col-xs-6 con-left-top"><img class="img-responsive" src="/{{$product->a_img_six}}"></div>
                                <div class="col-md-6 col-sm-6 col-xs-6 "><img class="img-responsive" src="/{{$product->a_img_seven}}"></div>
                                <div class="col-md-6 col-sm-6 col-xs-6 con-left-top"><img class="img-responsive" src="/{{$product->a_img_eight}}"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="page2" class="page section">
        <h2 class="fcn text-center">{{$product->productName}}</h2>
        <h1 class="fcn text-center">{{$product->chateau}}</h1>
        <h3 class="fcn text-center" style="margin-bottom: 10px">{{$product->productName_en}}</h3>
        <div class="container content">
            <div class="row">
                <div class="col-md-3 left wine hidden-xs hidden-sm">
                    <img class="img-responsive" src="/{{$product->productImage}}" >
                </div>
                <div class="col-md-9 right wine-content">
                    <div class="row">
                      <div class="col-md-6 col-sm-6 col-my col-mylg">
                      
                        </div>
                        <div class="col-md-6 col-sm-6 col-my col-mylg02">
                            <h4>产品信息</h4>
                         
                        </div>
                        <div class="col-md-6 col-sm-6 col-my col-mylg">
                            <!-- <h4 style="text-indent: -9999px">产品信息</h4> -->
                            <img class="img-responsive" src="/images/img5.jpg" width="100%">
                        </div>
                        <div class="col-md-6 col-sm-6 col-my col-mylg02">
                            <p>[产品名称] {{$product->productName}}</p>
                            <p>[葡萄品种] {{$product->grape_variety}}</p>
                            <p>[葡萄采摘年份] {{$product->grape_year}}</p>
                            <p>[酒精浓度] {{$product->alcohol_concentration}}</p>
                            <p>[橡木桶陈酿时间] {{$product->brew_time}}</p>
                            <p>[瓶中熟成時間] {{$product->ripening_time}}</p>
                            <p>[色泽成色] {{$product->ripening_color}}</p>
                        </div>
                    </div>
                    <div class="row  Glory">
                        <div class="col-md-12">
                            <h4 class="fcn">酒庄<span>荣耀</span></h4>
                        </div>
                        <div class="col-md-4 col-pic">
                            <img class="img-responsive" src="/{{$product->glory_img_one}}">
                            
                            <h4 class="text-center page02-top-font">{{$product->glory_tittle_one}}</h4>
                            <p class="text-center page02-bottom-font">{{$product->glory_content_one}}</p>
                        </div>
                        <div class="col-md-4 col-pic">
                            <img class="img-responsive" src="/{{$product->glory_img_two}}">
                        
                            <h4 class="text-center page02-top-font">{{$product->glory_tittle_two}}</h4>
                            <p class="text-center page02-bottom-font">{{$product->glory_content_two}}</p>
                        </div>
                        <div class="col-md-4 col-pic">
                            <img class="img-responsive" src="/{{$product->glory_img_three}}">
                        
                            <h4 class="text-center page02-top-font">{{$product->glory_tittle_three}}</h4>
                            <p class="text-center page02-bottom-font">{{$product->glory_content_three}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="page3" class="page section">
        <h2 class="fcn text-center">{{$product->productName}}</h2>
        <h1 class="fcn text-center">{{$product->chateau}}</h1>
        <h3 class="fcn text-center" style="margin-bottom: 10px">{{$product->productName_en}}</h3>
        <div class="container content">
            <div class="row">
                <div class="col-md-9 wine-content">
                    <div class="row mover-foor">
                        <div class="col-md-12">
                            <h3>配餐建议</h3>
                        </div>
                        <div class="col-md-3 col-xs-6">
                            <img class="img-responsive pic-width" src="/{{$product->catering_img_one}}">
                            <h4>{{$product->catering_title_one}}</h4>
                        </div>
                        <div class="col-md-3  col-xs-6">
                            <img class="img-responsive pic-width" src="/{{$product->catering_img_two}}">
                            <h4>{{$product->catering_title_two}}</h4>
                        </div>
                        <div class="col-md-3  col-xs-6">
                            <img class="img-responsive pic-width" src="/{{$product->catering_img_three}}">
                            <h4>{{$product->catering_title_three}}</h4>
                        </div>
                        <div class="col-md-3 col-xs-6">
                            <img class="img-responsive pic-width" src="/{{$product->catering_img_four}}">
                            <h4>{{$product->catering_title_four}}</h4>
                        </div>
                    </div>
                    <div class="row tasting">
                        <div class="col-md-7 my-col-md">
                            <div class="row ">
                                <div class="col-md-12">
                                    <h3>品鉴与储存</h3>
                                </div>
                                <div class="col-md-6  col-xs-6">
                                    <div class="row mover-foor">
                                        <div class="col-md-4 col-xs-4">
                                            <img class="img-responsive" src="/images/ico1.jpg">
                                        </div>
                                        <div class="col-md-8 col-xs-8"><p><span style="white-space: nowrap">最佳品尝温度：</span> <br>{{$product->taste_temperature}} </p></div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-6">
                                    <div class="row ">
                                        <div class="col-md-4 col-xs-4">
                                            <img class="img-responsive" src="/images/ico2.jpg">
                                        </div>
                                        <div class="col-md-8 col-xs-8"><p><span style="white-space: nowrap">建议醒酒时间: </span><br>{{$product->sober_time}}</p></div>
                                    </div>
                                </div>
                                <div class="col-md-6  col-xs-6">
                                    <div class="row mover-foor">
                                        <div class="col-md-4 col-xs-4">
                                            <img class="img-responsive" src="/images/ico4.jpg">
                                        </div>
                                        <div class="col-md-8 col-xs-8"><p><span style="white-space: nowrap">储存方式：</span><br>{{$product->save_way}}</p></div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-6">
                                    <div class="row">
                                        <div class="col-md-4 col-xs-4">
                                            <img class="img-responsive" src="/images/ico3.jpg">
                                        </div>
                                        <div class="col-md-8 col-xs-8"><p><span style="white-space: nowrap">适饮期建议：</span><br>{{$product->drink_date}}</p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 my-col-content">
                            <div class="desc">
                                <div class="content">
                                    {{$product->introduce}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 wine hidden-xs hidden-sm col-md-jiu">
                    <img class="img-responsive" src="/{{$product->productImage}}">
                </div>
            </div>
        </div>
    </section>
    <section id="page4" class="page section" style="padding-bottom: 400px">
        <div class="container content">
            <div class="row">
                <div class="col-md-12 "  ><img class="img-responsive center-block bottom-pic-tp" src="/{{$product->qc_image}}" ></div>
            </div>
        </div>
    </section>
</div>
<div class="hidden" id="nav">page1</div>
<script type="text/javascript" src="/assets/fullpage/jquery.fullpage.min.js"></script>
<script type="text/javascript">
var page = '{{$page}}';
var MainHref = '{{$page_url}}';
$(function(){
    var wid = $(window).width();
    if (wid > 768) {
        $('#fullpage').fullpage({
            anchors: ['firstPage', 'secondPage', '3rdPage', '4thpage', 'lastPage'],
            scrollingSpeed: 1000
        });
    }
    if (wid < 768) {
        var str = ['firstPage', 'secondPage', '3rdPage', '4thpage', 'lastPage'];
        $(window).bind('scroll', function(e) {
            $('.page').each(function() {
                if ($(this).offset().top < window.pageYOffset + 10 && $(this).offset().top + $(this).height() > window.pageYOffset + 10) {
                    window.history.pushState({}, "Title", MainHref + "#" + str[$(this).index()]);
                }
            });
        });
    }
})
</script>
</body>
</html>