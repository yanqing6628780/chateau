<?php

namespace App\Http\Controllers;

use App\Wine;
use App\Http\Controllers\Controller as BaseController;

class ProductController extends BaseController
{
    function __construct() 
    {

    }

    public function getId($id, $page = 'page1')
    {
        $data['page_url'] = url('product/id/'.$id);
        $data['page'] = $page;
        $data['product'] = Wine::find($id);
        return view('product', $data);
    }

    public function getBarcode($code, $page = 'page1')
    {
        $data['page'] = $page;
        $data['page_url'] = url('product/barcode/'.$code);
        $data['product'] = Wine::where('barcode', $code)->first();
        if($data['product']){
            return view('product', $data);
        }else{
            return abort(404);
        }
    }
}
