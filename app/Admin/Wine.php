<?php

Admin::model('App\Wine')->title('酒')->display(function ()
{
  $display = AdminDisplay::table();
  $display->columns([
    Column::string('barcode')->label('条形码'),
    Column::string('productName')->label('产品名'),
    Column::image('productImage')->label('产品图'),
    Column::action('show')->label('前台地址')->value('访问')->url(function ($instance)
    {
        return url('product/barcode', [$instance->barcode]);
    })->target('_blank'),
  ]);
  return $display;
})->createAndEdit(function ()
{
  $form = AdminForm::form();
  $form->items([
    FormItem::text('barcode', '条形码')->required()->unique(),
    FormItem::text('productName', '产品名')->required(),
    FormItem::text('productName_en', '产品名-英文')->required(),
    FormItem::text('chateau', '庄园')->required(),
    FormItem::image('productImage', '产品图')->required(),
    FormItem::text('country','国家'),
    FormItem::text('produceArea','产区'),
    FormItem::text('level','级别'),
    FormItem::image('a_img_one','地图')->required(),
    FormItem::image('a_img_two','page1-图1')->required(),
    FormItem::image('a_img_three','page1-图2')->required(),
    FormItem::image('a_img_four','page1-图3')->required(),
    FormItem::image('a_img_five','page1-图4')->required(),
    FormItem::image('a_img_six','page1-图5')->required(),
    FormItem::image('a_img_seven','page1-图6')->required(),
    FormItem::image('a_img_eight','page1-图7')->required(),
    FormItem::text('grape_variety', '葡萄品种'),
    FormItem::text('grape_year', '葡萄采摘年份'),
    FormItem::text('alcohol_concentration', '酒精浓度'),
    FormItem::text('brew_time', '橡木桶陈酿时间'),
    FormItem::text('ripening_time', '瓶中熟成時間'),
    FormItem::text('ripening_color', '色泽成色'),
    FormItem::image('glory_img_one', '荣耀-图1')->required(),
    FormItem::text('glory_tittle_one', '荣耀-图1-标题'),
    FormItem::text('glory_content_one', '荣耀-图1-内容'),
    FormItem::image('glory_img_two', '荣耀-图2')->required(),
    FormItem::text('glory_tittle_two', '荣耀-图2-标题'),
    FormItem::text('glory_content_two', '荣耀-图2-内容'),
    FormItem::image('glory_img_three', '荣耀-图3')->required(),
    FormItem::text('glory_title_three', '荣耀-图3-标题'),
    FormItem::text('glory_content_three', '荣耀-图3-内容'),
    FormItem::image('catering_img_one', '配餐-图1')->required(),
    FormItem::text('catering_title_one', '配餐-图1-文字'),
    FormItem::image('catering_img_two', '配餐-图2')->required(),
    FormItem::text('catering_title_two', '配餐-图2-文字'),
    FormItem::image('catering_img_three', '配餐-图3')->required(),
    FormItem::text('catering_title_three', '配餐-图3-文字'),
    FormItem::image('catering_img_four', '配餐-图4')->required(),
    FormItem::text('catering_title_four', '配餐-图4-文字'),
    FormItem::text('taste_temperature', '最佳品尝温度'),
    FormItem::text('sober_time', '建议醒酒时间'),
    FormItem::text('save_way', '储存方式'),
    FormItem::text('drink_date', '适饮期建议'),
    FormItem::textarea('introduce', '简介'),
    FormItem::image('qc_image', '质检单'),
  ]);
  return $form;
});